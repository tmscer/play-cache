package com.typesafe.play.redis

import javax.inject.{Inject, Singleton}
import akka.Done
import play.api.cache.{AsyncCacheApi, SyncCacheApi}

import scala.concurrent.{Await, ExecutionContext, ExecutionContextExecutor, Future}
import scala.concurrent.duration.Duration
import scala.reflect.ClassTag

@Singleton
class RedisAsyncCacheApi @Inject()(val syncCache: RedisCacheApi) extends AsyncCacheApi {

  val executionContext: ExecutionContextExecutor = ExecutionContext.global

  override def set(key: String, value: Any, expiration: Duration): Future[Done] = Future {
    syncCache.set(key, expiration)
    Done.getInstance()
  }(executionContext)

  override def remove(key: String): Future[Done] = Future {
    syncCache.remove(key)
    Done.getInstance()
  }(executionContext)

  override def getOrElseUpdate[A](key: String, expiration: Duration)(orElse: => Future[A])(implicit evidence$1: ClassTag[A]): Future[A] = Future {
    val defaultValue: A = Await.result[A](orElse, Duration.Inf)
    syncCache.getOrElseUpdate[A](key, expiration)(defaultValue)(evidence$1)
  }(executionContext)

  override def get[T](key: String)(implicit evidence$2: ClassTag[T]): Future[Option[T]] = Future {
    syncCache.get[T](key)(evidence$2)
  }(executionContext)

  override def removeAll(): Future[Done] = Future {
    syncCache.removeAll()
    Done.getInstance()
  }(executionContext)

  override lazy val sync: SyncCacheApi = syncCache
}
