package com.typesafe.play.redis;

import com.google.inject.AbstractModule;
import org.sedis.Pool;
import play.api.cache.AsyncCacheApi;
import play.api.cache.SyncCacheApi;
import redis.clients.jedis.JedisPool;

public class ASyncRedisCacheModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(SyncCacheApi.class).to(RedisCacheApi.class);
        bind(AsyncCacheApi.class).to(RedisAsyncCacheApi.class);

        bind(JedisPool.class).toProvider(JedisPoolProvider.class);
        bind(Pool.class).toProvider(SedisPoolProvider.class);
    }

}
